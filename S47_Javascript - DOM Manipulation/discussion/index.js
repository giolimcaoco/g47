/*
	Using DOM (Document Object Model)

		Previously, we are using the following codes in terms of selecting elements inside the html file

			// document refer to the whole webpage and the getElementId selects the element with the same id as the text inside its arguments
				document.getElementId(`txt-first-name`);
				document.getElementByClassName(`txt-last-name`); //by class
				document.getElementByTagName(`input`); //by tag
*/

// querySelector replaces the three getElement selectors and makes use of CSS format in terms of selecting the elements inside the html as its argument (# - id, . - class, tagName - tag)
document.querySelector(`#txt-first-name`);

const txtFirstName = document.querySelector('#txt-first-name')
const txtLastName = document.querySelector('#txt-last-name')
const spanFullName = document.querySelector('#span-full-name')

// Event Listeners
// events are all interactions of the user to our webpage; such examples are clicing, hovering, reloading, keypress, keyup(typing)

/*
	the function() used is the addEventListerner - allows a block of codes to listen to an event for them to be executed

	addEventListener takes two arguments: 
		1. a string that identifies the event to which the codes will listen to
		2. a function() that the listener will execute once the specified event is triggered
	
*/
txtFirstName.addEventListener('keyup', (event) =>{
	// innerHTML - this allows the element to record/duplicate the value of the selected variable (txtFirstName.value)
	// .value is needed since without it, the .innerHTML will only record what type of element the target variable is inside the HTML document instead of getting its value
	spanFullName.innerHTML = txtFirstName.value;
})

// multiple listeners can also be assigned to same event; in the same way, same listeners can listen to different events
txtFirstName.addEventListener('keyup', (event) =>{
	// even contains the information the triggered event passed from the first argument
	// event.target contains the element where the event happened
	// event.target.value - gets the value of the input object where the event happened
	console.log(event.target);
	console.log(event.target.value);
})


